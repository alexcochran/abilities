#!/bin/bash
# .devcontainer/post-create-commands.sh
#
# Extension script containing shell commands that are run once the container has been created.
# --------------------------------------------------------------------------------------------

# Get the latest versions of dependencies and update the `poetry.lock` file. Only dependencies
# without specific version constraints will be updated.
poetry update

# Install depdendencies
poetry install
