# abilities

An overengineered service that facilitates storing a list of abilities/skills.

## Usage

To run the project with Gunicorn, use `poetry run gunicorn -w 4 "abilities:create_app()"`.

## Resources

- [Demystifying Flask's "Application Factory"](https://hackersandslackers.com/flask-application-factory/)
- [SQLAlchemy 2.0 Documentation](https://docs.sqlalchemy.org/en/20/index.html)
- [Pure SQLAlchemy + Flask example](https://github.com/Mirantis/kostyor/blob/master/kostyor/db/api.py)
