"""Auth resources.

https://www.youtube.com/watch?v=AsQ8OcVvK3U
https://github.com/Vuka951/tutorial-code/blob/master/flask-jwt-auth/main.py
"""

from flask import jsonify, make_response, Response, Flask, request
from flask_restful import Resource
from flask_jwt_extended import create_access_token, JWTManager
import bcrypt
from sqlalchemy.exc import IntegrityError

from abilities.models import User
from abilities import database


def init_jwt(app: Flask) -> JWTManager:
    """Initialize a JWTManager object for the application."""

    return JWTManager(app=app)


class Register(Resource):
    """User registration resource."""

    def post(self) -> Response:
        """Register a new user."""

        try:
            email = request.json.get("email", None)
            username = request.json.get("username", None)
            password = request.json.get("password", None)

            if not email:
                return make_response(jsonify("Missing email address"), 400)
            if not username:
                return make_response(jsonify("Missing username"), 400)
            if not password:
                return make_response(jsonify("Missing password"), 400)

            # Hash the provided password and create a new User object
            hashed_password = bcrypt.hashpw(password=password.encode("utf-8"), salt=bcrypt.gensalt())
            user = User(email=email, username=username, hashed_password=hashed_password)

            # Write the new User to the database
            with database.session() as current_session:
                with current_session.begin():
                    try:
                        current_session.add(user)
                        current_session.commit()
                    except IntegrityError:
                        return make_response("User already exists", 400)

            # Return an access token for the new user
            access_token = create_access_token(identity=username)
            return make_response(jsonify(access_token=access_token), 200)
        except AttributeError:
            return make_response("Provide an Email and Password in JSON format in the request body", 400)


class Login(Resource):
    """User login resource."""

    def post(self) -> Response:
        """Generate a JWT if a valid username and password are provided."""

        username = request.json.get("username", None)
        password = request.json.get("password", None)

        if not username:
            return make_response(jsonify("Missing username"), 400)
        if not password:
            return make_response(jsonify("Missing password"), 400)

        # Invalid
        if username != "test" or password != "test":
            return make_response(jsonify({"msg": "Invalid username or password"}), 401)

        access_token = create_access_token(identity=args["username"])
        return make_response(jsonify(access_token=access_token), 200)
