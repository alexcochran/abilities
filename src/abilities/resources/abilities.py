"""Abilities resources."""

from uuid import UUID

from flask import jsonify, make_response, Response, request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from sqlalchemy import and_
from sqlalchemy.sql.expression import select, update
from sqlalchemy.exc import IntegrityError, SQLAlchemyError

from abilities import models, schemas, database


class Ability(Resource):
    """Resource for one Ability."""

    # @jwt_required()
    def get(self, user_guid: UUID, ability_guid: UUID) -> Response:
        """Retrieve one Ability."""

        with database.session() as current_session:
            ability = current_session.execute(
                select(models.Ability).where(
                    and_(models.Ability.guid == ability_guid, models.Ability.user_guid == user_guid)
                )
            )
        return make_response(schemas.ability_schema.dump(ability), 200)

    def put(self, user_guid: UUID, ability_guid: UUID) -> Response:
        """Update an Ability."""

        ability_name = request.json.get("name", None)
        ability_description = request.json.get("description", None)

        if not ability_name:
            return make_response(jsonify("Missing ability name"), 400)
        if not ability_description:
            return make_response(jsonify("Missing ability description"), 400)

        with database.session() as current_session:
            result = current_session.execute(
                update(models.Ability)
                .where(and_(models.Ability.guid == ability_guid, models.Ability.user_guid == user_guid))
                .values(name=ability_name, description=ability_description)
            )

            current_session.commit()

        return make_response(schemas.ability_schema.dump(result), 200)


class Abilities(Resource):
    """Resource for many abilities."""

    # @jwt_required()
    def get(self, user_guid: UUID) -> Response:
        """Retrieve all abilities for a user."""

        with database.session() as current_session:
            with current_session.begin():
                try:
                    result = current_session.execute(
                        select(models.Ability).where(models.Ability.user_guid == user_guid)
                    )
                # TODO: replace with a less generic error
                except SQLAlchemyError:
                    return make_response("Database error", 400)

        return make_response(schemas.abilities_schema.dump(result.all()))

    # @jwt_required()
    def post(self, user_guid: UUID) -> Response:
        """Create an Ability."""

        post_objs: list = []

        for obj in request.json:
            ability_name = obj.get("name", None)
            ability_description = obj.get("description", None)

            if not ability_name:
                return make_response(jsonify("Missing ability name"), 400)
            if not ability_description:
                return make_response(jsonify("Missing ability description"), 400)

            post_objs.append(models.Ability(user_guid=user_guid, name=ability_name, description=ability_description))

        print(post_objs)

        # Write the new Ability to the database
        with database.session() as current_session:
            with current_session.begin():
                try:
                    current_session.bulk_add_objects(post_objs)
                    current_session.commit()
                except Exception as exc:
                    print(exc)
                # except IntegrityError as exc:
                #     return make_response(f"Ability already exists: {exc}", 400)
                # # TODO: replace with a less generic error
                # except SQLAlchemyError as exc:
                #     return make_response(f"Database error: {exc}", 400)

        return make_response(schemas.abilities_schema.dump(post_objs), 200)
