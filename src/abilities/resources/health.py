"""Health check resources."""

import json

from flask import jsonify, Response
from flask_restful import Resource
from healthcheck import HealthCheck, EnvironmentDump
from flask_jwt_extended import jwt_required


class Health(Resource):
    """Health check resource."""

    @jwt_required()
    def get(self) -> Response:
        """Retrieve the current health status."""

        health_check = HealthCheck()
        health_data = health_check.run()[0]

        return jsonify(json.loads(health_data))


class Environment(Resource):
    """Environment dump resource."""

    @jwt_required()
    def get(
        self,
    ) -> Response:
        """Dump information about the application environment."""

        env_dump_obj = EnvironmentDump()
        env_dump_data = env_dump_obj.run()[0]

        return jsonify(json.loads(env_dump_data))
