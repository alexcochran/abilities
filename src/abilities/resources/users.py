"""User resources."""

from uuid import UUID

from flask import make_response, Response
from flask_restful import Resource
from sqlalchemy import select

from abilities import database, models, schemas


class User(Resource):
    """Resource for one User."""

    # @jwt_required()
    def get(self, user_guid: str) -> Response:
        """Retrieve one User."""

        print(user_guid)

        with database.session() as current_session:
            user = current_session.query(models.User).get(user_guid)

        return make_response(schemas.user_schema.dump(user), 200)
