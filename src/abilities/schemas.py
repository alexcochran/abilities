"""Schemas for handling data serialization/deserialization.

https://marshmallow.readthedocs.io/en/stable/quickstart.html
"""

import warnings

# https://github.com/marshmallow-code/flask-marshmallow/issues/53
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    from flask_marshmallow import Marshmallow


marsh = Marshmallow()


class UserSchema(marsh.Schema):
    """Schema for User data."""

    class Meta:
        """Marshmallow Schema metaclass."""

        # Fields to expose
        fields = ("guid", "email", "username")


user_schema = UserSchema()
users_schema = UserSchema(many=True)


class AbilitySchema(marsh.Schema):
    """Schema for Ability data."""

    class Meta:
        """Marshmallow Schema metaclass."""

        # Fields to expose
        fields = ("guid", "name", "description", "_links")

    # Smart hyperlinking
    _links = marsh.Hyperlinks(
        {"self": marsh.URLFor("ability", values=dict(id="<guid>")), "collection": marsh.URLFor("abilities")}
    )


ability_schema = AbilitySchema()
abilities_schema = AbilitySchema(many=True)
