"""Custom encoders for the application to help with serialization."""

from typing import Any
from uuid import UUID
import json


class UUIDEncoder(json.JSONEncoder):
    """Custom json encoder to be able to fix the 'UUID('…') is not JSON serializable' issue.

    https://stackoverflow.com/questions/68792701/serialize-uuid-objects-in-flask-restful
    """

    def default(self, obj: Any) -> Any:  # pylint:disable=arguments-renamed
        if isinstance(obj, UUID):
            return str(obj)  # <- notice I'm not returning obj.hex as the original answer
        return json.JSONEncoder.default(self, obj)
