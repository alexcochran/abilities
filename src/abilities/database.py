"""Database connection setup.

https://flask.palletsprojects.com/en/2.3.x/patterns/sqlalchemy
https://docs.sqlalchemy.org/en/20/orm/contextual.html#unitofwork-contextual
"""

import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker, DeclarativeBase

# from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.dialects.postgresql import UUID
import uuid


engine = sqlalchemy.create_engine("sqlite:///abilities.sqlite3", echo=True)
session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


class GUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses PostgreSQL's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.

    https://docs.sqlalchemy.org/en/20/core/custom_types.html#backend-agnostic-guid-type
    """

    impl = CHAR
    cache_ok = True

    def load_dialect_impl(self, dialect):
        if dialect.name == "postgresql":
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == "postgresql":
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value


class Base(DeclarativeBase):
    type_annotation_map = {
        uuid.UUID: GUID,
    }


def init_database() -> None:
    """Initialize the database with the application configuration and models."""

    import abilities.models  # noqa # pylint:disable=unused-import,import-outside-toplevel

    Base.metadata.create_all(bind=engine)
