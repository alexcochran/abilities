"""API data models."""

import uuid
from typing import List

from sqlalchemy import String, Uuid, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from abilities.database import Base, GUID


class User(Base):
    """Data model for an application user."""

    __tablename__ = "user"

    guid: Mapped[uuid.UUID] = mapped_column(primary_key=True, default=uuid.uuid4)
    username: Mapped[str] = mapped_column(String(30))
    email: Mapped[str] = mapped_column(String(255))
    hashed_password: Mapped[str] = mapped_column(String(255))

    abilities: Mapped[List["Ability"]] = relationship(back_populates="user", cascade="all, delete-orphan")

    def __repr__(self) -> str:
        """String representation of a User."""

        return f"{self.__class__.__name__}(uuid={self.guid!r}, username={self.username!r}, email={self.email!r})"


class Ability(Base):
    """Data model for an ability/skill."""

    __tablename__ = "ability"

    guid: Mapped[uuid.UUID] = mapped_column(primary_key=True, default=uuid.uuid4)
    name: Mapped[str] = mapped_column(String(255))
    description: Mapped[str] = mapped_column(String(255))

    user_guid: Mapped[uuid.UUID] = mapped_column(ForeignKey("user.guid"))
    user: Mapped["User"] = relationship(back_populates="abilities")

    def __repr__(self) -> str:
        """String representation of an Ability."""

        return f"{self.__class__.__name__}(uuid={self.guid!r}, name={self.name!r}, description={self.description!r}), user_guid={self.user_guid}"
