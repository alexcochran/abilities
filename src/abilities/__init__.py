"""Basic Flask RESTful API implementation."""

from flask import Flask
from flask_restful import Api

# from flask_marshmallow import Marshmallow
from flasgger import Swagger, swag_from

# from flask_sqlalchemy import SQLAlchemy

from abilities.resources import auth, abilities, users, health
from abilities import database

# from abilities.resources.auth import jwt
from abilities.config import AppConfig
from abilities.schemas import marsh

# marsh = Marshmallow()


def create_app() -> Flask:
    """Application factory function."""

    app = Flask(__name__)
    api = Api(app=app)
    # marshmallow = Marshmallow(app=app)
    app.config.from_object(AppConfig)

    # Initialize the JWTManager
    _ = auth.init_jwt(app=app)

    # Initialize the Swagger API documentation
    _ = Swagger(app)

    # Initialize plugins

    # Perform tasks within the application context:
    with app.app_context():
        # Create the tables
        database.init_database()

        # Initialize the Marshmallow plugin
        marsh.init_app(app)

        # Add API routes:
        # ---------------
        api.add_resource(auth.Register, "/register")
        api.add_resource(auth.Login, "/login")

        api.add_resource(users.User, "/user/<user_guid>")

        api.add_resource(abilities.Ability, "/user/<user_guid>/abilities/<ability_guid>")
        api.add_resource(abilities.Abilities, "/user/<user_guid>/abilities")

        api.add_resource(health.Health, "/health")
        api.add_resource(health.Environment, "/environment")
        # ---------------

    return app


if __name__ == "__main__":
    application = create_app()
    application.run(debug=True)
