"""Application settings."""

import os
from dataclasses import dataclass

from abilities import encoders


@dataclass
class AppConfig:
    """Application-wide configuration and settings."""

    VERSION = os.environ.get("VERSION")
    JWT_SECRET_KEY = "itT$f7mB*4%ZVtaWAvj@TAnsANNqXiiC"  # os.environ.get("JWT_SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = "sqlite://abilities.sqlite3"
    RESTFUL_JSON = {"cls": encoders.UUIDEncoder}


@dataclass
class FlaskRestfulConfig:
    """Flask-RESTful custom application configuration.

    https://flask-restful.readthedocs.io/en/latest/extending.html
    """

    RESTFUL_JSON = {"cls": encoders.UUIDEncoder}
